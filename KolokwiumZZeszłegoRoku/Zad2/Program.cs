﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<AGD> DlaDomu = new List<AGD>();
            DlaDomu.Add(new Mikrofalówka("Electrolux"));
            DlaDomu.Add(new Czajnik("Electrolux", new DateTime(2010, 2, 12)));
            DlaDomu.Add(new Pralka("Bosch", new DateTime(2011, 6, 2)));
            DlaDomu.Sort();

            Console.WriteLine("W domu jest:");
            foreach (AGD przedmiot in DlaDomu)
            {
                Console.WriteLine(przedmiot.ToString());
            }

            Console.ReadKey();
        }
    }

    abstract class AGD : IComparable<AGD>
    {
        private bool stan;
        private string typ;
        private string nazwa;
        private string producent;
        private DateTime data_produkcji;

        public AGD(string typ, string nazwa, string producent, DateTime data_produkcji)
        {
            this.typ = typ;
            this.nazwa = nazwa;
            this.producent = producent;
            this.data_produkcji = data_produkcji;
        }

        public AGD(string typ, string nazwa, string producent)
            : this(typ, nazwa, producent, DateTime.Now) { }

        public virtual bool StanUrządzenia
        {
            get { return stan; }
        }

        public virtual void Start()
        {
            stan = true;
        }

        public virtual void Stop()
        {
            stan = false;
        }

        public override string ToString()
        {
            return string.Format("Urzadzenie {0}, o nazwie {1}, wyprodukowane przez {2} dnia {3}. Stan: {4}",
                typ, nazwa, producent, data_produkcji.ToString(), stan ? "działa" : "nie działa");
        }

        public int CompareTo(AGD obj)
        {
            return DateTime.Compare(obj.data_produkcji, data_produkcji);
        }
    }

    class Łazienkowe : AGD
    {
        public Łazienkowe(string nazwa, string producent, DateTime data_produkcji) : base("Łazienkowe", nazwa, producent, data_produkcji) { }
        public Łazienkowe(string nazwa, string producent) : base("Łazienkowe", nazwa, producent) { }
    }

    class Kuchenne : AGD
    {
        public Kuchenne(string nazwa, string producent, DateTime data_produkcji) : base("Kuchenne", nazwa, producent, data_produkcji) { }
        public Kuchenne(string nazwa, string producent) : base("Kuchenne", nazwa, producent) { }
    }

    class Czajnik : Kuchenne
    {
        public Czajnik(string producent, DateTime data_produkcji) : base("Czajnik", producent, data_produkcji) { }
        public Czajnik(string producent) : base("Czajnik", producent) { }
    }

    class Mikrofalówka : Kuchenne
    {
        public Mikrofalówka(string producent, DateTime data_produkcji) : base("Mikrofalówka", producent, data_produkcji) { }
        public Mikrofalówka(string producent) : base("Mikrofalówka", producent) { }
    }

    class Pralka : Łazienkowe
    {
        public Pralka(string producent, DateTime data_produkcji) : base("Pralka", producent, data_produkcji) { }
        public Pralka(string producent) : base("Pralka", producent) { }
    }

}
