﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad1
{
    class Program
    {
        static void Main(string[] args)
        {
            Table characters = new Table();
            Table characters_copy = null;
            bool exit = false;
            do
            {
                Console.Clear();
                Console.WriteLine("Opcje:\n1. Uzupełnij\n2. Wypisz\n3. Skopiuj\n4. Wypisz kopię\n5. Wyjdź");
                int choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.Write("\nPodaj znaki, którymi uzupełnić tablicę: ");
                        characters.Fill(Console.ReadLine());
                        break;
                    case 2:
                        Console.WriteLine("\nStworzona tablica:");
                        for (int i = 0; i < characters.Length; i++)
                        {
                            Console.Write(characters[i]);
                        }

                        Console.ReadKey();
                        break;
                    case 3:
                        characters_copy = (Table)characters.Clone();
                        break;
                    case 4:
                        Console.WriteLine("\nKopia tablicy:");
                        for (int i = 0; i < characters_copy.Length; i++)
                        {
                            Console.Write(characters_copy[i]);
                        }

                        Console.ReadKey();
                        break;
                    case 5:
                        exit = true;
                        break;
                    default:
                        break;
                }
            } while (exit == false);
        }
    }

    class Table : ICloneable
    {
        public char[] tab;

        public Table()
        {
            tab = new char[20];
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = '_';
            }
        }

        public int Length
        {
            get { return tab.Length; }
        }

        public char this[int i]
        {
            get { return tab[i]; }
            set { tab[i] = value; }
        }

        public void Fill(string text)
        {
            if (text.Length > tab.Length)
            {
                throw new Exception("Za długi tekst!");
            }
            else
            {
                for (int i = 0; i < text.Length; i++)
                {
                    tab[i] = text[i];
                }
            }
        }

        public object Clone()
        {
            Table table_clone = new Table();
            for (int i = 0; i < tab.Length; i++)
            {
                table_clone[i] = tab[i];
            }

            return table_clone;
        }
    }
}
