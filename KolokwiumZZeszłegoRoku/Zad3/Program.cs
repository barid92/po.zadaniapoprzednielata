﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad3
{
    class Program
    {
        static void Main(string[] args)
        {
            string test = "TEST";
            Console.WriteLine(test.Ile());
            Console.WriteLine(test.Odwróć());
            Console.WriteLine(test.Skróć(2));
            Console.WriteLine(test.Usuń('T'));

            Console.ReadKey();

        }
    }

    static class ExtendedString
    {
        public static int Ile(this string s)
        {
            return s.Length;
        }

        public static string Odwróć(this string s)
        {
            string result = "";
            for (int i = s.Length - 1; i >= 0; i--)
            {
                result += s[i];
            }

            return result;
        }

        public static string Skróć(this string s, int ile_skrócić)
        {
            string result = "";
            for (int i = 0; i < s.Length - ile_skrócić; i++)
            {
                result += s[i];
            }

            return result;
        }

        public static string Usuń(this string s, char do_usunięcia)
        {
            string result = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (s[i] != do_usunięcia)
                    result += s[i];
            }

            return result;
        }
    }
}
